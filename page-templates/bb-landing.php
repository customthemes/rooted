<?php

/**
 * This file adds the full landing template to the rooted Theme to use with Beaver Builder.
 *
 * @package      Rooted
 * @subpackage   Customizations
 * @link         http://riotcutoms.com
 * @author       Katrina M. // Riot Customs LLC
 * @copyright    Copyright (c) 2020
 * @license      GPL-2.0+
 */


/*
Template Name: Beaver Builder
*/

// Add custom body class to the head
add_filter( 'body_class', 'rooted_add_body_class' );
function rooted_add_body_class( $classes ) {
   $classes[] = 'rooted-bb';
   return $classes;
}

// Remove header, navigation, breadcrumbs, footer widgets, footer
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Remove site header elements
remove_action( 'genesis_before', 'rooted_offscreen_content_output', 1 );
remove_action( 'genesis_after_header', 'rooted_before_content', 15 );

//* Remove navigation
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
remove_action( 'genesis_before', 'genesis_do_subnav', 12 );
remove_action( 'genesis_before', 'genesis_do_nav', 2 );
remove_action( 'genesis_before', 'genesis_do_nav', 10 );
remove_action( 'genesis_after', 'rooted_footer_menu', 11 );
unregister_sidebar( 'header-right' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove site footer widgets
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );


genesis();
