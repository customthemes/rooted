<?php
/**
 * Rooted Theme.
 *
 * This file adds the Customizer additions to the Rooted Theme Theme.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://www.riotcustoms.com/
 */

add_action( 'customize_register', 'rooted_customizer_register' );
/**
 * Registers settings and controls with the Customizer.
 *
 * @since 2.2.3
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function rooted_customizer_register( $wp_customize ) {

	$wp_customize->add_setting(
		'rooted_link_color',
		array(
			'default'           => rooted_customizer_get_default_link_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'rooted_link_color',
			array(
				'description' => __( 'Change the color of post info links and button blocks, the hover color of linked titles and menu items, and more.', 'rooted-theme' ),
				'label'       => __( 'Link Color', 'rooted-theme' ),
				'section'     => 'colors',
				'settings'    => 'rooted_link_color',
			)
		)
	);

	$wp_customize->add_setting(
		'rooted_accent_color',
		array(
			'default'           => rooted_customizer_get_default_accent_color(),
			'sanitize_callback' => 'sanitize_hex_color',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'rooted_accent_color',
			array(
				'description' => __( 'Change the default hover color for button links, menu buttons, and submit buttons. The button block uses the Link Color.', 'rooted-theme' ),
				'label'       => __( 'Accent Color', 'rooted-theme' ),
				'section'     => 'colors',
				'settings'    => 'rooted_accent_color',
			)
		)
	);

	$wp_customize->add_setting(
		'rooted_logo_width',
		array(
			'default'           => 350,
			'sanitize_callback' => 'absint',
		)
	);

	// Add a control for the logo size.
	$wp_customize->add_control(
		'rooted_logo_width',
		array(
			'label'       => __( 'Logo Width', 'rooted-theme' ),
			'description' => __( 'The maximum width of the logo in pixels.', 'rooted-theme' ),
			'priority'    => 9,
			'section'     => 'title_tagline',
			'settings'    => 'rooted_logo_width',
			'type'        => 'number',
			'input_attrs' => array(
				'min' => 75,
			),

		)
	);

}
