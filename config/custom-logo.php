<?php
/**
 * Rooted Theme child theme.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://riotcustoms.com/
 */

/**
 * Custom Logo configuration.
 */
return array(
	'height'      => 120,
	'width'       => 700,
	'flex-height' => true,
	'flex-width'  => true,
);
