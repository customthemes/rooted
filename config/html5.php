<?php
/**
 * Rooted Theme child theme.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://riotcustoms.com/
 */

/**
 * Elements to output as html5.
 */
return array(
	'caption',
	'comment-form',
	'comment-list',
	'gallery',
	'search-form',
);
