<?php
/**
 * Rooted Theme theme settings.
 *
 * Genesis 2.9+ updates these settings when themes are activated.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://www.riotcustoms.com/
 */

return array(
	GENESIS_SETTINGS_FIELD => genesis_get_config( 'child-theme-settings-genesis' ),
	'posts_per_page'       => 6,
);
