<?php
/**
 * Block Editor settings specific to Rooted Theme.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://www.riotcustoms.com/
 */

$rooted_link_color            = get_theme_mod( 'rooted_link_color', rooted_customizer_get_default_link_color() );
$rooted_link_color_contrast   = rooted_color_contrast( $rooted_link_color );
$rooted_link_color_brightness = rooted_color_brightness( $rooted_link_color, 35 );

return array(
	'admin-fonts-url'              => 'https://fonts.googleapis.com/css?family=Merriweather|Roboto&display=swap',
	'content-width'                => 1200,
	'default-button-bg'            => $rooted_link_color,
	'default-button-color'         => $rooted_link_color_contrast,
	'default-button-outline-hover' => $rooted_link_color_brightness,
	'default-link-color'           => $rooted_link_color,
	'editor-color-palette'         => array(
		array(
			'name'  => __( 'Green', 'rooted-theme' ),
			'slug'  => 'green',
			'color'	=> '#11582d',
		),
		array(
			'name'  => __( 'Plum', 'rooted-theme' ),
			'slug'  => 'plum',
			'color' => '#801d59',
		),
		array(
			'name'  => __( 'Gold', 'rooted-theme' ),
			'slug'  => 'gold',
			'color' => '#f5af50',
	       ),
		 array(
	 		'name'  => __( 'Rose', 'rooted-theme' ),
	 		'slug'  => 'rose',
	 		'color' => '#efc7cb',
	 	),
	 	array(
	 		'name'  => __( 'Dirt', 'rooted-theme' ),
	 		'slug'  => 'dirt',
	 		'color' => '#f1e3d4',
	        ),
	),
	'editor-font-sizes'            => array(
		array(
			'name' => __( 'Small', 'rooted-theme' ),
			'size' => 12,
			'slug' => 'small',
		),
		array(
			'name' => __( 'Normal', 'rooted-theme' ),
			'size' => 18,
			'slug' => 'normal',
		),
		array(
			'name' => __( 'Large', 'rooted-theme' ),
			'size' => 20,
			'slug' => 'large',
		),
		array(
			'name' => __( 'Larger', 'rooted-theme' ),
			'size' => 24,
			'slug' => 'larger',
		),
	),
);
