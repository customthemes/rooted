<?php
/**
 * Rooted Theme child theme.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://riotcustoms.com/
 */

/**
 * Genesis Accessibility features to support.
 */
return array(
	'404-page',
	'drop-down-menu',
	'headings',
	'search-form',
	'skip-links',
);
