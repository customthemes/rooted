<?php
/**
 * Rooted Theme child theme.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://riotcustoms.com/
 */

/**
 * Supported Genesis navigation menus.
 */
return array(
	'primary'   => __( 'Header Menu', 'rooted-theme' ),
	'secondary' => __( 'Footer Menu', 'rooted-theme' ),
);
