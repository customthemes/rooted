<?php
/**
 * Rooted Theme Simple Social Icons default settings.
 *
 * @package Rooted Theme
 * @author  Riot Customs LLC
 * @license GPL-2.0-or-later
 * @link    https://www.riotcustoms.com/
 */

return array(
	'alignment'              => 'alignleft',
	'background_color'       => '#f5f5f5',
	'background_color_hover' => '#333333',
	'border_radius'          => 3,
	'border_width'           => 0,
	'icon_color'             => '#333333',
	'icon_color_hover'       => '#ffffff',
	'size'                   => 40,
);
